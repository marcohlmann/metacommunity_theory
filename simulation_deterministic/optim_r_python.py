## Generate a contour plot
# Import some other libraries that we'll need
# matplotlib and numpy packages must also be installed
import numpy as np
import random
import math
import os
import sys


kmin = int(sys.argv[1])
kmax = int(sys.argv[2])

print(kmin)
print(kmax)


os.chdir("YOURPATH/simulation/params/")

# define objective function 


#################################################
# Simulated Annealing
##################################################


def simul_anneal_graph(init,A_c_csv,A_e_csv):
    
    A_c = np.genfromtxt(A_c_csv,delimiter=",",skip_header=1) 
    A_e = np.genfromtxt(A_e_csv,delimiter=",",skip_header=1) 
    M = np.sum(A_c)
    
    def g(y):
        C = np.dot(A_c,y)
        E = (1-(np.dot(A_e,y))/M)
        return C/E
        
    def h(y):
        return -min(((1-y)/y)*g(y))
    
    def clip(x):
        return max(min(x,1.0),0.000000001)
        # Start location
        
    x_start =  np.genfromtxt(init, delimiter=",", skip_header=1)    

    # Number of cycles
    n = 10#20000
    # Number of trials per cycle
    m = 100
    # Number of accepted solutions
    na = 0.0
    # Probability of accepting worse solution at the start
    #p1 = 0.7
    p1=0.001
    
    # Probability of accepting worse solution at the end
    #p50 = 0.001
    p50 = 0.0001
    #initial size of the support of the proposal distribution
    r_init = 0.005
    #final size of the support of the proposal distribution
    r_final = 0.000001
    # Initial temperature
    t1 = -1.0/math.log(p1)
    # Final temperature
    t50 = -1.0/math.log(p50)
    # Fractional reduction every cycle
    frac = (t50/t1)**(1.0/(n-1.0))
    # Initialize x
    x = np.zeros((n+1,A_c.shape[1]))
    x[0] = x_start
    xi = x_start
    na = na + 1.0
    
    # Current best results so far
    xstore = np.zeros((m,A_c.shape[1]))
    xc = np.zeros(A_c.shape[1])
    xc = x[0]
    fc = h(xi)
    fs = np.zeros(n+1)
    fs[0] = fc
    # Current temperature
    t = t1
    
    clip_vec = np.vectorize(clip)
    

   # DeltaE Average
    DeltaE_avg = 0.0
    for i in range(n):

        acceptstore = np.ones(m, dtype=bool)
        hvaluestore = np.zeros(m)
        if  (np.floor_divide(i,100) == i/100):
            print('Cycle: ' + str(i) + ' with Temperature: ' + str(t))
        for j in range(m):
            # Generate new trial points
            #mu, sigma = 0, (r_final-r_init)*i/(n-1)+r_init # mean and standard deviation
            s = np.random.uniform(-((r_final-r_init)*i/(n-1)+r_init), (r_final-r_init)*i/(n-1)+r_init, A_c.shape[1])
            #coeff = (r_final-r_init)*i/(n-1)+r_init
            #s = np.random.multivariate_normal(np.zeros(A_c.shape[1]),  coeff*np.absolute(np.diag(np.gradient(g(x_start), edge_order=1))))
                
            xi = xc + s
            #xi = xc + np.random.uniform(-((r_final-r_init)*i/(n-1)+r_init),(r_final-r_init)*i/(n-1)+r_init,A_c.shape[1])        
            # Clip to upper and lower bounds
            xi = clip_vec(xi)
            #store the proposed values
            xstore[j] = xi
            hvaluestore[j] = h(xi)
            #xi[1] = max(min(xi[1],1.0),0)
            DeltaE = abs(h(xi)-fc)        
            DeltaE_avg = DeltaE
   
            if (h(xi)>fc):
                # Initialize DeltaE_avg if a worse solution was found
                #   on the first iteration
                if (i==0 and j==0): DeltaE_avg = DeltaE
                # objective function is worse
                # generate probability of acceptance
                p = math.exp(-DeltaE/(DeltaE_avg * t))
                # determine whether to accept worse point
                if (random.random()<p):
                    # accept the worse solution
                    acceptstore[j] = True
                else:
                    # don't accept the worse solution
                    acceptstore[j] = False
            else:
                # objective function is lower, automatically accept
                acceptstore[j] = True
         # update currently accepted solution
            xc = xstore[np.argmin(hvaluestore)]            
            fc = hvaluestore[np.argmin(hvaluestore)]
            # increment number of accepted solutions
            na = na + 1.0
                # update DeltaE_avg
            DeltaE_avg = (DeltaE_avg * (na-1.0) +  DeltaE) / na
        # Record the best x values at the end of every cycle
        x[i+1] = xc
        fs[i+1] = fc
        # Lower the temperature for next cycle
        t = frac * t


    return x[::5],fs[::5]

def simul_anneal_graph_parallel(k):
    return simul_anneal_graph('x_init_'+str(k)+'.csv','A_c_'+str(k)+'.csv','A_e_'+str(k)+'.csv')

def simul_anneal_graph_lev_parallel(k):
    return simul_anneal_graph('x_init_lev_'+str(k)+'.csv','A_c_lev_'+str(k)+'.csv','A_e_lev_'+str(k)+'.csv')

def simul_anneal_graph_comb_parallel(k):
    return simul_anneal_graph('x_init_comb_'+str(k)+'.csv','A_c_comb_'+str(k)+'.csv','A_e_comb_'+str(k)+'.csv')

def simul_anneal_graph_sep_parallel(k):
    return simul_anneal_graph('x_init_sep_'+str(k)+'.csv','A_c_sep_'+str(k)+'.csv','A_e_sep_'+str(k)+'.csv')

def simul_anneal_graph_resc_parallel(k):
    return simul_anneal_graph('x_init_resc_'+str(k)+'.csv','A_c_resc_'+str(k)+'.csv','A_e_resc_'+str(k)+'.csv')

for k in range(kmin,kmax+1):
    print(k)
    res = simul_anneal_graph_parallel(k)
    np.savetxt('../results/x_SA_'+str(k),res[0])
    np.savetxt('../results/lambda_M_SA_'+str(k),res[1])
    
