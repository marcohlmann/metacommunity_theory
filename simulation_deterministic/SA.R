#This script aims to compute metacommunity capacities of the four different models 

#Due to simulation annealhing in high dimension space, it takes time ! We encourage doing on a cluster

#For running it on a cluster
#args=(commandArgs(TRUE))
#k_min = as.numeric(args[[1]])
#k_max = as.numeric(args[[2]])
#print(k_min)

library(reticulate)
library(parallel)
library(parallel)
PATH = 'YOURPATH'


setwd(PATH)

#source the simulating anneahiling in python
source_python("./simulation/optim_r_python.py")
#run the SA in parallel
res_SA = mclapply(k_min:k_max,FUN = simul_anneal_graph_parallel,mc.cores = 4)
setwd(PATH)
save(res_SA,file = paste0('simulation/results/res_SA_',k_min,'_',k_max,'.Rdata'))

